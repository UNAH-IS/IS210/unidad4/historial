﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EnvioDemo
{
    public partial class FrmCliente : Form
    {
        private const string RUTA = "C:\\POOL\\";
        private string peticion;
        private string respuesta;

        public FrmCliente()
        {
            InitializeComponent();
            LblInfo.Text = string.Empty;
        }

        public void EscribirPeticion()
        {
            this.peticion = this.respuesta = RUTA + DateTime.Now.ToString("s").Replace(':', '_');
            this.peticion += ".in";
            this.respuesta += ".out";

            var sw = new StreamWriter(this.peticion, false);
            sw.WriteLine(CmbOperacion.Text);
            sw.WriteLine(TxtCuenta.Text);
            sw.Close();
        }

        private void BtnConsultar_Click(object sender, EventArgs e)
        {
            // Validar que esten los valores requeridos
            if (CmbOperacion.Text.Equals(string.Empty) || TxtCuenta.Text.Equals(string.Empty))
            {
                return;
            }

            EscribirPeticion();
            
            BtnConsultar.Enabled = false;
            timer1.Enabled = true;
        }

        private void MostrarRespuesta(string[] campos)
        {
            // Valida que hayan valores en la respuesta
            if (campos.Length == 0)
            {
                return;
            }
            
            // Promedio
            if (CmbOperacion.SelectedIndex == 0)
            {
                TxtRespuesta.Text = $"El promedio es: {campos[0]}";
            }
            else if (CmbOperacion.SelectedIndex == 1)
            {
                TxtRespuesta.Text = "Las clases aprobadas son: ";

                foreach(string asignaturas in campos)
                {
                    TxtRespuesta.Text += asignaturas + "\n";
                }
            } 
            else if (CmbOperacion.SelectedIndex == 2)
            {
                TxtRespuesta.Text = "Las clases reprobadas son: ";

                foreach (string asignaturas in campos)
                {
                    TxtRespuesta.Text += asignaturas + "\n";
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            LblInfo.Text = "Esperando respuesta...";

            if (File.Exists(this.respuesta))
            {
                LblInfo.Text = "Archivo hallado...";

                string contenido = File.ReadAllText(this.respuesta);
                TxtRespuesta.Text = contenido;
                BtnConsultar.Enabled = true;
            }
        }
    }
}
