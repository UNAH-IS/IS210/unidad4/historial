﻿namespace EnvioDemo
{
    partial class FrmCliente
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbOperacion = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCuenta = new System.Windows.Forms.TextBox();
            this.BtnConsultar = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TxtRespuesta = new System.Windows.Forms.TextBox();
            this.LblInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operacion";
            // 
            // CmbOperacion
            // 
            this.CmbOperacion.FormattingEnabled = true;
            this.CmbOperacion.Items.AddRange(new object[] {
            "Promedio",
            "Aprobados",
            "Reprobados"});
            this.CmbOperacion.Location = new System.Drawing.Point(117, 54);
            this.CmbOperacion.Name = "CmbOperacion";
            this.CmbOperacion.Size = new System.Drawing.Size(218, 24);
            this.CmbOperacion.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cuenta:";
            // 
            // TxtCuenta
            // 
            this.TxtCuenta.Location = new System.Drawing.Point(117, 103);
            this.TxtCuenta.Name = "TxtCuenta";
            this.TxtCuenta.Size = new System.Drawing.Size(218, 22);
            this.TxtCuenta.TabIndex = 4;
            // 
            // BtnConsultar
            // 
            this.BtnConsultar.Location = new System.Drawing.Point(360, 99);
            this.BtnConsultar.Name = "BtnConsultar";
            this.BtnConsultar.Size = new System.Drawing.Size(86, 31);
            this.BtnConsultar.TabIndex = 5;
            this.BtnConsultar.Text = "Consultar";
            this.BtnConsultar.UseVisualStyleBackColor = true;
            this.BtnConsultar.Click += new System.EventHandler(this.BtnConsultar_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TxtRespuesta
            // 
            this.TxtRespuesta.AcceptsReturn = true;
            this.TxtRespuesta.Location = new System.Drawing.Point(28, 157);
            this.TxtRespuesta.Multiline = true;
            this.TxtRespuesta.Name = "TxtRespuesta";
            this.TxtRespuesta.Size = new System.Drawing.Size(418, 263);
            this.TxtRespuesta.TabIndex = 6;
            // 
            // LblInfo
            // 
            this.LblInfo.Location = new System.Drawing.Point(25, 439);
            this.LblInfo.Name = "LblInfo";
            this.LblInfo.Size = new System.Drawing.Size(421, 17);
            this.LblInfo.TabIndex = 7;
            this.LblInfo.Text = "label3";
            // 
            // FrmCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 466);
            this.Controls.Add(this.LblInfo);
            this.Controls.Add(this.TxtRespuesta);
            this.Controls.Add(this.BtnConsultar);
            this.Controls.Add(this.TxtCuenta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CmbOperacion);
            this.Controls.Add(this.label1);
            this.Name = "FrmCliente";
            this.Text = "Cliente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbOperacion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtCuenta;
        private System.Windows.Forms.Button BtnConsultar;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox TxtRespuesta;
        private System.Windows.Forms.Label LblInfo;
    }
}

