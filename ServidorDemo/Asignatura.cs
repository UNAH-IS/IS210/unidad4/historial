﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecepcionDemo
{
    class Asignatura
    {
        private const int NOTA_MINIMA_APROBADO = 65;

        public string CodigoClase { get; }
        public string CuentaAlumno { get; }
        public int Nota { get; }
        public int UV { get; }
        public int Promedio { get; set; }

        public Asignatura(string codigoClase, string cuentaAlumno, int nota, int uv)
        {
            this.CodigoClase = codigoClase;
            this.CuentaAlumno = cuentaAlumno;
            this.Nota = nota;
            this.UV = uv;
        }

        public bool EstaAprobado()
        {
            return (this.Nota >= NOTA_MINIMA_APROBADO);
            
        }
    }

}
