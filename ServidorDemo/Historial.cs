﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecepcionDemo
{
    class Historial
    {
        List<Asignatura> asignaturas;

        public Historial()
        {
            asignaturas = new List<Asignatura>();
        }

        public Historial(string[] lineas)
        {
            asignaturas = new List<Asignatura>();

            try
            {
                foreach (string linea in lineas)
                {
                    string[] campos = linea.Split(',');

                    string codigo = campos[0];
                    string cuenta = campos[1];
                    int nota = int.Parse(campos[2]);
                    int uv = int.Parse(campos[3]);

                    // Agrega una asignatura al listado
                    asignaturas.Add(new Asignatura(codigo, cuenta, nota, uv));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CalcularPromedio(string cuentaAlumno)
        {
            int totalUV = 0;
            int totalUVPorNota = 0;
            decimal total = 0;
            int resultado = 0;

            foreach (Asignatura asignatura in asignaturas)
            {
                if (asignatura.CuentaAlumno.Equals(cuentaAlumno))
                {
                    totalUV += asignatura.UV;
                    totalUVPorNota += asignatura.UV * asignatura.Nota;
                }
            }

            if (totalUV > 0)
            {
                total = totalUVPorNota / totalUV;
                resultado = (int)Math.Round(total, 0);
            }

            return resultado;
        }

        public string[] GetAprobados(string cuentaAlumno)
        {
            List<string> aprobados = new List<string>();

            foreach (Asignatura asignatura in asignaturas)
            {
                if (asignatura.CuentaAlumno.Equals(cuentaAlumno))
                {
                    if (asignatura.EstaAprobado())
                    {
                        aprobados.Add(asignatura.CodigoClase);
                    }
                }
            }

            return aprobados.ToArray();
        }

        public string[] GetReprobados(string cuentaAlumno)
        {
            List<string> reprobados = new List<string>();

            foreach (Asignatura asignatura in asignaturas)
            {
                if (asignatura.CuentaAlumno.Equals(cuentaAlumno))
                {
                    if (!asignatura.EstaAprobado())
                    {
                        reprobados.Add(asignatura.CodigoClase);
                    }
                }
            }

            return reprobados.ToArray();
        }
    }
}
