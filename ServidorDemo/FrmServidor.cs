﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RecepcionDemo
{
    public partial class FrmServidor : Form
    {
        private const string RUTA = "c:\\POOL\\";

        private const string ARCHIVO_DATA = @"c:\POOL\data.txt";

        private Historial historial;

        public FrmServidor()
        {
            InitializeComponent();

            try
            {
                this.historial = new Historial(File.ReadAllLines(ARCHIVO_DATA));
            }
            catch(Exception)
            {
                this.historial = new Historial();
            }

            timer1.Enabled = true;
            timer2.Enabled = true;
        }

        private void EscribirRespuesta(string origen, string destino)
        {
            using (StreamReader sr = new StreamReader(origen))
            {
                string operacion = sr.ReadLine();
                string cuenta = sr.ReadLine();

                switch (operacion)
                {
                    case "Promedio":
                        int promedio = this.historial.CalcularPromedio(cuenta);
                        File.WriteAllText(destino, promedio.ToString());
                        break;
                    case "Aprobados":
                        string[] aprobados = this.historial.GetAprobados(cuenta);
                        File.WriteAllLines(destino, aprobados);
                        break;
                    case "Reprobados":
                        string[] reprobados = this.historial.GetReprobados(cuenta);
                        File.WriteAllLines(destino, reprobados);
                        break;
                }
            }            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var archivos = Directory.EnumerateFiles(RUTA, "*.in");

            foreach(string archivo in archivos)
            {
                if (listBox1.Items.IndexOf(archivo) < 0)
                {
                    listBox1.Items.Add(archivo);
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            foreach (string archivo in listBox1.Items)
            {
                string respuesta = archivo.Replace(".in", ".out");

                if (listBox2.Items.IndexOf(respuesta) < 0)
                {
                    listBox2.Items.Add(respuesta);
                    EscribirRespuesta(archivo, respuesta);
                }

                File.Delete(archivo);
            }
        }
    }
}
