# Historial

Manejo del historial de un alumno a través de la transmisión de archivos entre dos programas distintos.

## Instrucciones
1. Descargar el proyecto.
2. Crear una carpeta ```C:\POOL\```, colocar el archivo **data.txt** dentro. 
3. Dentro de la solución hay dos proyectos: Ambos deben compilarse.
4. Para esto se dirigen a la vista **Explorador de Soluciones**, le dan clic derecho sobre cualquiera de los dos proyectos y se van a la opción **Establecer como proyecto de inicio**. Cuando presionan el botón **Iniciar**, compilan el proyecto de inicio. 
5. Esto debe realizarse para *ClienteDemo* y para *ServidorDemo*.
6. Luego debe dirigirse a la carpeta donde se guarda el ejecutable de cada proyecto. Están ubicadas en: ```ServidorDemo/bin/Debug``` y ```ClienteDemo/bin/Debug```, en ambas se encontrarán un archivo EXE el cual deben ejecutar.
7. Una vez iniciado el Servidor ya es posible utilizar el Cliente para realizar consultas
